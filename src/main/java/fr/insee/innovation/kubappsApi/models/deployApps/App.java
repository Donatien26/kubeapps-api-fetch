package fr.insee.innovation.kubappsApi.models.deployApps;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "chartMetadata", "releaseName", "namespace", "icon", "version", "chart", "status" })
public class App {

    @JsonProperty("chartMetadata")
    private ChartMetadata chartMetadata;
    @JsonProperty("releaseName")
    private String releaseName;
    @JsonProperty("namespace")
    private String namespace;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("version")
    private String version;
    @JsonProperty("chart")
    private String chart;
    @JsonProperty("status")
    private String status;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("chartMetadata")
    public ChartMetadata getChartMetadata() {
        return chartMetadata;
    }

    @JsonProperty("chartMetadata")
    public void setChartMetadata(ChartMetadata chartMetadata) {
        this.chartMetadata = chartMetadata;
    }

    @JsonProperty("releaseName")
    public String getReleaseName() {
        return releaseName;
    }

    @JsonProperty("releaseName")
    public void setReleaseName(String releaseName) {
        this.releaseName = releaseName;
    }

    @JsonProperty("namespace")
    public String getNamespace() {
        return namespace;
    }

    @JsonProperty("namespace")
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("chart")
    public String getChart() {
        return chart;
    }

    @JsonProperty("chart")
    public void setChart(String chart) {
        this.chart = chart;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    // @JsonAnyGetter
    // public Map<String, Object> getAdditionalProperties() {
    // return this.additionalProperties;
    // }

    // @JsonAnySetter
    // public void setAdditionalProperty(String name, Object value) {
    // this.additionalProperties.put(name, value);
    // }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("chartMetadata", chartMetadata).append("releaseName", releaseName)
                .append("namespace", namespace).append("icon", icon).append("version", version).append("chart", chart)
                .append("status", status).append("additionalProperties", additionalProperties).toString();
    }

}
