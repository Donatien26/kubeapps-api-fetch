
package fr.insee.innovation.kubappsApi.models.charts;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "latestChartVersion", "chart" })
@JsonIgnoreProperties(ignoreUnknown = true)
public class Relationships {

    @JsonProperty("latestChartVersion")
    private LatestChartVersion latestChartVersion;

    @JsonProperty("chart")
    private ChartVersion chartVersion;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("latestChartVersion")
    public LatestChartVersion getLatestChartVersion() {
        return latestChartVersion;
    }

    @JsonProperty("latestChartVersion")
    public void setLatestChartVersion(LatestChartVersion latestChartVersion) {
        this.latestChartVersion = latestChartVersion;
    }

    /**
     * @return the chartVersion
     */
    @JsonProperty("chart")
    public ChartVersion getChartVersion() {
        return chartVersion;
    }

    /**
     * @param chartVersion the chartVersion to set
     */
    @JsonProperty("chart")
    public void setChartVersion(ChartVersion chartVersion) {
        this.chartVersion = chartVersion;
    }
    // @JsonAnyGetter
    // public Map<String, Object> getAdditionalProperties() {
    // return this.additionalProperties;
    // }

    // @JsonAnySetter
    // public void setAdditionalProperty(String name, Object value) {
    // this.additionalProperties.put(name, value);
    // }

}
