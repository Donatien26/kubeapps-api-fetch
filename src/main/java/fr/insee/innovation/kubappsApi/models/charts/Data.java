
package fr.insee.innovation.kubappsApi.models.charts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "version", "app_version", "created", "digest", "urls", "readme", "values", "schema" })
public class Data {

    @JsonProperty("version")
    private String version;
    @JsonProperty("app_version")
    private String appVersion;
    @JsonProperty("created")
    private String created;
    @JsonProperty("digest")
    private String digest;
    @JsonProperty("urls")
    private List<String> urls = null;
    @JsonProperty("readme")
    private String readme;
    @JsonProperty("values")
    private String values;
    @JsonProperty("schema")
    private String schema;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("app_version")
    public String getAppVersion() {
        return appVersion;
    }

    @JsonProperty("app_version")
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    @JsonProperty("digest")
    public String getDigest() {
        return digest;
    }

    @JsonProperty("digest")
    public void setDigest(String digest) {
        this.digest = digest;
    }

    @JsonProperty("urls")
    public List<String> getUrls() {
        return urls;
    }

    @JsonProperty("urls")
    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    @JsonProperty("readme")
    public String getReadme() {
        return readme;
    }

    @JsonProperty("readme")
    public void setReadme(String readme) {
        this.readme = readme;
    }

    @JsonProperty("values")
    public String getValues() {
        return values;
    }

    @JsonProperty("values")
    public void setValues(String values) {
        this.values = values;
    }

    @JsonProperty("schema")
    public String getSchema() {
        return schema;
    }

    @JsonProperty("schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }

    // @JsonAnyGetter
    // public Map<String, Object> getAdditionalProperties() {
    // return this.additionalProperties;
    // }

    // @JsonAnySetter
    // public void setAdditionalProperty(String name, Object value) {
    // this.additionalProperties.put(name, value);
    // }

}
