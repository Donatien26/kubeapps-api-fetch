package fr.insee.innovation.kubappsApi.models.deployApps;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang.builder.ToStringBuilder;

import fr.insee.innovation.kubappsApi.models.commons.Maintainer;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "home", "sources", "version", "description", "keywords", "maintainers", "icon",
        "apiVersion", "appVersion" })
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChartMetadata {

    @JsonProperty("name")
    private String name;
    @JsonProperty("home")
    private String home;
    @JsonProperty("sources")
    private List<String> sources = null;
    @JsonProperty("version")
    private String version;
    @JsonProperty("description")
    private String description;
    @JsonProperty("keywords")
    private List<String> keywords = null;
    @JsonProperty("maintainers")
    private List<Maintainer> maintainers = null;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("apiVersion")
    private String apiVersion;
    @JsonProperty("appVersion")
    private String appVersion;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("home")
    public String getHome() {
        return home;
    }

    @JsonProperty("home")
    public void setHome(String home) {
        this.home = home;
    }

    @JsonProperty("sources")
    public List<String> getSources() {
        return sources;
    }

    @JsonProperty("sources")
    public void setSources(List<String> sources) {
        this.sources = sources;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("keywords")
    public List<String> getKeywords() {
        return keywords;
    }

    @JsonProperty("keywords")
    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    @JsonProperty("maintainers")
    public List<Maintainer> getMaintainers() {
        return maintainers;
    }

    @JsonProperty("maintainers")
    public void setMaintainers(List<Maintainer> maintainers) {
        this.maintainers = maintainers;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    @JsonProperty("apiVersion")
    public String getApiVersion() {
        return apiVersion;
    }

    @JsonProperty("apiVersion")
    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    @JsonProperty("appVersion")
    public String getAppVersion() {
        return appVersion;
    }

    @JsonProperty("appVersion")
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    // @JsonAnyGetter
    // public Map<String, Object> getAdditionalProperties() {
    // return this.additionalProperties;
    // }

    // @JsonAnySetter
    // public void setAdditionalProperty(String name, Object value) {
    // this.additionalProperties.put(name, value);
    // }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("home", home).append("sources", sources)
                .append("version", version).append("description", description).append("keywords", keywords)
                .append("maintainers", maintainers).append("icon", icon).append("apiVersion", apiVersion)
                .append("appVersion", appVersion).append("additionalProperties", additionalProperties).toString();
    }

}
