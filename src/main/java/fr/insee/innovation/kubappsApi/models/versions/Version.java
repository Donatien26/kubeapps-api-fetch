package fr.insee.innovation.kubappsApi.models.versions;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang.builder.ToStringBuilder;

import fr.insee.innovation.kubappsApi.models.charts.Data;
import fr.insee.innovation.kubappsApi.models.charts.Links;
import fr.insee.innovation.kubappsApi.models.charts.Relationships;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "relationships", "attributes", "links", "id", "type" })
@JsonIgnoreProperties(ignoreUnknown = true)
public class Version {

    @JsonProperty("relationships")
    private Relationships relationships;
    @JsonProperty("attributes")
    private Data attributes;
    @JsonProperty("links")
    private Links links;
    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("relationships")
    public Relationships getRelationships() {
        return relationships;
    }

    @JsonProperty("relationships")
    public void setRelationships(Relationships relationships) {
        this.relationships = relationships;
    }

    @JsonProperty("attributes")
    public Data getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(Data attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("links")
    public Links getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(Links links) {
        this.links = links;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    // @JsonAnyGetter
    // public Map<String, Object> getAdditionalProperties() {
    // return this.additionalProperties;
    // }

    // @JsonAnySetter
    // public void setAdditionalProperty(String name, Object value) {
    // this.additionalProperties.put(name, value);
    // }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("relationships", relationships).append("attributes", attributes)
                .append("links", links).append("id", id).append("type", type)
                .append("additionalProperties", additionalProperties).toString();
    }

}
