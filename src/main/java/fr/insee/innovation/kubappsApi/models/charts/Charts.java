
package fr.insee.innovation.kubappsApi.models.charts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "data" })
@JsonIgnoreProperties(ignoreUnknown = true)
public class Charts {

    @JsonProperty("data")
    private List<Chart> charts = null;
    @JsonIgnore
    private final Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("data")
    public List<Chart> getData() {
        return charts;
    }

    @JsonProperty("data")
    public void setData(final List<Chart> data) {
        this.charts = data;
    }

    // @JsonAnyGetter
    // public Map<String, Object> getAdditionalProperties() {
    // return this.additionalProperties;
    // }

    // @JsonAnySetter
    // public void setAdditionalProperty(final String name, final Object value) {
    // this.additionalProperties.put(name, value);
    // }
}