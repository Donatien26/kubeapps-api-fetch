
package fr.insee.innovation.kubappsApi.models.charts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import fr.insee.innovation.kubappsApi.models.commons.Maintainer;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "ID", "name", "repo", "description", "home", "keywords", "maintainers", "sources", "icon",
        "raw_icon", "icon_content_type", "chartVersions" })
public class Attributes {

    @JsonProperty("ID")
    private String iD;
    @JsonProperty("name")
    private String name;
    @JsonProperty("repo")
    private Repo repo;
    @JsonProperty("description")
    private String description;
    @JsonProperty("home")
    private String home;
    @JsonProperty("keywords")
    private List<String> keywords = null;
    @JsonProperty("maintainers")
    private List<Maintainer> maintainers = null;
    @JsonProperty("sources")
    private List<String> sources = null;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("raw_icon")
    private Object rawIcon;
    @JsonProperty("icon_content_type")
    private String iconContentType;
    @JsonProperty("chartVersions")
    private List<Object> chartVersions = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ID")
    public String getID() {
        return iD;
    }

    @JsonProperty("ID")
    public void setID(String iD) {
        this.iD = iD;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("repo")
    public Repo getRepo() {
        return repo;
    }

    @JsonProperty("repo")
    public void setRepo(Repo repo) {
        this.repo = repo;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("home")
    public String getHome() {
        return home;
    }

    @JsonProperty("home")
    public void setHome(String home) {
        this.home = home;
    }

    @JsonProperty("keywords")
    public List<String> getKeywords() {
        return keywords;
    }

    @JsonProperty("keywords")
    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    @JsonProperty("maintainers")
    public List<Maintainer> getMaintainers() {
        return maintainers;
    }

    @JsonProperty("maintainers")
    public void setMaintainers(List<Maintainer> maintainers) {
        this.maintainers = maintainers;
    }

    @JsonProperty("sources")
    public List<String> getSources() {
        return sources;
    }

    @JsonProperty("sources")
    public void setSources(List<String> sources) {
        this.sources = sources;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    @JsonProperty("raw_icon")
    public Object getRawIcon() {
        return rawIcon;
    }

    @JsonProperty("raw_icon")
    public void setRawIcon(Object rawIcon) {
        this.rawIcon = rawIcon;
    }

    @JsonProperty("icon_content_type")
    public String getIconContentType() {
        return iconContentType;
    }

    @JsonProperty("icon_content_type")
    public void setIconContentType(String iconContentType) {
        this.iconContentType = iconContentType;
    }

    @JsonProperty("chartVersions")
    public List<Object> getChartVersions() {
        return chartVersions;
    }

    @JsonProperty("chartVersions")
    public void setChartVersions(List<Object> chartVersions) {
        this.chartVersions = chartVersions;
    }

    // @JsonAnyGetter
    // public Map<String, Object> getAdditionalProperties() {
    // return this.additionalProperties;
    // }

    // @JsonAnySetter
    // public void setAdditionalProperty(String name, Object value) {
    // this.additionalProperties.put(name, value);
    // }

}
