package fr.insee.innovation.kubappsApi.controller;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.insee.innovation.kubappsApi.models.charts.Charts;
import fr.insee.innovation.kubappsApi.models.deployApps.App;
import fr.insee.innovation.kubappsApi.models.versions.Version;
import fr.insee.innovation.kubappsApi.service.KubeappsCall;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * RepositoryController
 */
@Tag(name = "My lab", description = "My services")
@RequestMapping("/repository")
@RestController
public class RepositoryController {
    @Autowired
    private KubeappsCall kubeapps;

    @GetMapping("/getCharts")
    public Charts getCharts() throws IOException {
        return kubeapps.getAvailableCharts();
    }

    @GetMapping("/getChartsInRepo")
    public Charts getChartsOfRepo(@RequestParam String reponame)
            throws JsonParseException, JsonMappingException, IOException {
        return kubeapps.getAvailableChartsInRepo(reponame);
    }

    @GetMapping("/getAppsInNamespace")
    public List<App> getAppOfNamespace(@RequestParam String namespace)
            throws JsonParseException, JsonMappingException, IOException {
        return kubeapps.getAppsInNamespace(namespace);
    }

    @GetMapping("/getAllAppsInCluster")
    public List<App> getAllAppsInCluster() throws JsonParseException, JsonMappingException, IOException {
        return kubeapps.getAllAppsInCluster();
    }

    @GetMapping("/getAppVersions")
    public List<Version> getVersionOfApp(@RequestParam String nameRepo, @RequestParam String appName)
            throws JsonParseException, JsonMappingException, IOException {
        return kubeapps.getVersionsOfApp(nameRepo, appName).getVersions();
    }

    @GetMapping("/getAppDescription")
    public String getDescriptionOfApp(@RequestParam String nameRepo, @RequestParam String appName,
            @RequestParam String version) {
        return kubeapps.getDescriptionofApp(nameRepo, appName, version);
    }

    @GetMapping("/getAppValue")
    public String getValueOfApp(@RequestParam String nameRepo, @RequestParam String appName,
            @RequestParam String version) {
        return kubeapps.getValuesOfApp(nameRepo, appName, version);
    }

    @PostMapping("/startApp")
    public String launchApp(@RequestParam String namespace, @RequestParam String data) {
        return kubeapps.postApp(namespace, data);
    }

}