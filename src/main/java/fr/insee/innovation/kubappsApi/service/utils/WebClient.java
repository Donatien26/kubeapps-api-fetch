package fr.insee.innovation.kubappsApi.service.utils;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * WebClient
 */
@Service
public class WebClient {

    @Value("${kubeapps.api.token}")
    private String KUBEAPPS_TOKEN;
    @Value("${kubeapps.api.uri}")
    private String KUBEAPPS_URI;

    public Response get(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(KUBEAPPS_URI + url).addHeader("Authorization", KUBEAPPS_TOKEN)
                .build();

        Call call = client.newCall(request);
        return call.execute();
    }

    public Response POST(String url, String JSON) throws IOException {
        OkHttpClient client = new OkHttpClient();
        MediaType json = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(json, JSON);
        Request request = new Request.Builder().url(url).post(body).build();
        try (Response response = client.newCall(request).execute()) {
            return response;
        }
    }
}