package fr.insee.innovation.kubappsApi.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.insee.innovation.kubappsApi.models.charts.Charts;
import fr.insee.innovation.kubappsApi.models.deployApps.App;
import fr.insee.innovation.kubappsApi.models.deployApps.Apps;
import fr.insee.innovation.kubappsApi.models.versions.Versions;
import fr.insee.innovation.kubappsApi.service.utils.WebClient;
import okhttp3.Response;

/**
 * kubeappsCall
 */
@Service
public class KubeappsCall {

    @Autowired
    WebClient client;

    public Charts getAvailableCharts() throws IOException {
        Response response = client.get("/assetsvc/v1/charts");
        ObjectMapper objectMapper = new ObjectMapper();
        InputStream json = response.body().byteStream();
        Charts helmRepo = objectMapper.readValue(json, Charts.class);
        return helmRepo;
    }

    public Charts getAvailableChartsInRepo(String repoName)
            throws JsonParseException, JsonMappingException, IOException {
        // https://kubeapps.eneman.fr/api/assetsvc/v1/charts/stable
        Response response = client.get("/assetsvc/v1/charts/" + repoName);
        ObjectMapper objectMapper = new ObjectMapper();
        InputStream json = response.body().byteStream();
        Charts helmRepo = objectMapper.readValue(json, Charts.class);
        return helmRepo;
    }

    public List<App> getAppsInNamespace(String namespace) throws JsonParseException, JsonMappingException, IOException {
        // https://kubeapps.eneman.fr/api/tiller-deploy/v1/namespaces/donatien/releases
        Response response = client.get("/tiller-deploy/v1/namespaces/" + namespace + "/releases");
        ObjectMapper objectMapper = new ObjectMapper();
        InputStream json = response.body().byteStream();
        Apps apps = objectMapper.readValue(json, Apps.class);
        return apps.getData();
    }

    public List<App> getAllAppsInCluster() throws JsonParseException, JsonMappingException, IOException {
        // https://kubeapps.eneman.fr/api/tiller-deploy/v1/releases
        Response response = client.get("/tiller-deploy/v1/releases");
        ObjectMapper objectMapper = new ObjectMapper();
        InputStream json = response.body().byteStream();
        Apps apps = objectMapper.readValue(json, Apps.class);
        return apps.getData();
    }

    public String getDescriptionofApp(String nameRepo, String appName, String version) {
        // https://kubeapps.eneman.fr/api/assetsvc/v1/assets/stable/rocketchat/versions/2.0.2/README.md
        return null;
    }

    public Versions getVersionsOfApp(String nameRepo, String appName)
            throws JsonParseException, JsonMappingException, IOException {
        // https://kubeapps.eneman.fr/api/assetsvc/v1/charts/stable/rocketchat/versions
        Response response = client.get("/assetsvc/v1/charts/" + nameRepo + "/" + appName + "/versions");
        ObjectMapper objectMapper = new ObjectMapper();
        InputStream json = response.body().byteStream();
        Versions versions = objectMapper.readValue(json, Versions.class);
        return versions;
    }

    public String getValuesOfApp(String nameRepo, String appName, String version) {
        // https://kubeapps.eneman.fr/api/assetsvc/v1/assets/stable/rocketchat/versions/2.0.2/values.yaml
        return null;
    }

    public String postApp(String namespace, String data) {
        // https://kubeapps.eneman.fr/api/tiller-deploy/v1/namespaces/donatien/releases
        return null;
    }

}